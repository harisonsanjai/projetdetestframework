/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utilitaire.ModelView;
import anotation.Anotation;

/**
 *
 * @author P14A_122_Njato
 */
public class Olona {
    String nom;
    String prenom;
    int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public String getNom(){
        return nom;  
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    public String getPrenom(){
        return prenom;  
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    
    @Anotation ("getPersonne")
    public ModelView getPersonne()
    {
        ModelView mv=new ModelView();
        mv.setUrl("olona.jsp");
        Olona[] ol=new Olona[2];
        for(int i=0;i<ol.length;i++)
        {
            ol[i]=new Olona();
        }
        ol[0].setNom("Ranjato");
        ol[0].setPrenom("Harison Sanjai");
        ol[0].setAge(15);
        ol[1].setNom(this.getNom());
        ol[1].setPrenom(this.getPrenom());
        ol[1].setAge(this.getAge());
        mv.setData(ol);
        return mv;
    }

    public Olona() {}

    public Olona(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
}
